//Pera Nicholls
//2145293
package linaralgebra;
public class Vector3D {
    private double x; 
    private double y;
    private double z;

    public Vector3D(double x, double y, double z){
        this.x=x;
        this.y=y;
        this.z=z;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        double sum=Math.pow(this.x, 2)+Math.pow(this.y, 2)+Math.pow(this.z, 2);
        return Math.sqrt(sum);
    }

    public double dotProduct(Vector3D vector){
        double dotProduct=this.x*vector.getX()+this.y*vector.getY()+this.z*vector.getZ();
        return dotProduct;
    }

    public Vector3D add(Vector3D vector){
        return new Vector3D((this.x+vector.getX()),(this.y+vector.getY()),(this.z+vector.getZ()));
    }
}
