//Pera Nicholls
//2145293
package linaralgebra;
import static org.junit.Assert.*;
import org.junit.Test;

public class Vector3dtests {
    @Test 
    public void testGetMethods(){
        Vector3D vector= new Vector3D(3,4,5);
        assertTrue(3==vector.getX());
        assertTrue(4==vector.getY());
        assertTrue(5==vector.getZ());
    }

    @Test
    public void testMagnitude(){
        Vector3D vector= new Vector3D(-3,-3,-3);
        assertTrue(Math.sqrt(27)==vector.magnitude());
    }

    @Test
    public void testDotProduct(){
        Vector3D vector1= new Vector3D(1, 1, 2);
        Vector3D vector2= new Vector3D(2, 3, 4);
        assertTrue(13==vector1.dotProduct(vector2));
    }

    @Test
    public void testAdd(){
        Vector3D vector1= new Vector3D(1, 1, 2);
        Vector3D vector2= new Vector3D(2, 3, 4);
        Vector3D vector3= vector1.add(vector2);
        assertTrue(3==vector3.getX());
        assertTrue(4==vector3.getY());
        assertTrue(6==vector3.getZ());
    }
}
